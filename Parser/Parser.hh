#ifndef PARSER_HH_
# define PARSER_HH_

	/* INCLUDE */
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <sstream>
#include "../types/OperandFactory.hpp"
#include "../instruction/instructions.hpp"

#include "System_error.hh"

	/* MACCRO */

/* ERRORS */
# define PRINT_MISSING_ARG(line) "abstractvm : missing argument arg type at line " + std::to_string(line)
# define PRINT_WRONG_ARG(line, str) "abstractvm : wrong arg have " + str + " at line " + std::to_string(line)
# define PRINT_COMMAND_NOT_FOUND(token, line) "abstractvm : command '" + token + "' unknown at line " + std::to_string(line) + " please use [ " + mylist(_operators) + ", " + mylist(_instructions) + "]"
# define PRINT_FILE_NOT_FOUND(filename) "abstractvm : file '" + filename + "' not found"
# define PRINT_INVALID_NBR(arg, line, type) "abstractvm : have '" + arg + "' with type " + type + " at line " + std::to_string(line)
# define PRINT_INVALID_NBR_ARG(token, line) "abstractvm: '" + token + "' Don't need arg at line " + std::to_string(line)
# define MISSING_EXIT "MISSING ARGUMENT EXIT"
# define MISSING_COMMA "MISSING COMMA"

# define FLOAT 3
# define DOUBLE 4

# define OPERATOR 1
# define INSTRUCTION 2

	/* CLASS */
class Parser
{
	public:
		Parser(const std::string &filename);
		Parser();
		~Parser();

		struct _tab_line
		{
			std::string str;
			size_t 		line;
		};

	protected:
		int 	openfile(const std::string &);
		std::vector<std::string> my_tok(const std::string &);
		int		check_line(const std::string &, const size_t &);
		void	check_instruction(const std::string &);
		int 	check_operators(const std::vector<std::string> &, const size_t &);
		int 	check_number(const std::vector<std::string> &, const size_t &, const size_t &);
		bool 	isdigitn(const std::string &);
		bool	isdigitz(const std::string &);
		void	begin_parsing(const std::vector<_tab_line> &);
		const char *mylist(const std::vector<std::string> &);
		std::string	epure_str(const std::string &);

		std::string _filename;
		std::ifstream _file;
		std::vector<std::string> _instructions;
		std::vector<std::string> _operators;
		std::vector<std::string> _types;
		int _error_line;
		std::string message_error;
		//OperandFactory fac;
};

#endif /* PARSER_HH_ */