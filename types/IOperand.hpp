#include <string>

#ifndef I_OPERAND_HPP_
# define I_OPERAND_HPP_

enum	eOperandType
{
	INT8,
	INT16,
	INT32,
	FLOAT,
	DOUBLE
};

class IOperand
{

protected:

	std::string	_string;
	int			_precision;

public:

	virtual ~IOperand(){};

	virtual std::string const & toString() const = 0; // Renvoie une string reprensentant l’instance

	virtual int getPrecision() const = 0; // Renvoie la precision du type de l’instance
	virtual eOperandType getType() const = 0; // Renvoie le type de l’instance. Voir plus bas

	virtual IOperand* operator+(const IOperand &rhs) const = 0; // Somme
	virtual IOperand* operator-(const IOperand &rhs) const = 0; // Difference
	virtual IOperand* operator*(const IOperand &rhs) const = 0; // Produit
	virtual IOperand* operator/(const IOperand &rhs) const = 0; // Quotient
	virtual IOperand* operator%(const IOperand &rhs) const = 0; // Modulo

};

#endif // I_OPERAND_HPP_
