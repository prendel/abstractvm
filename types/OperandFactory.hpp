#include <map>
#include <string>
#include "IOperand.hpp"

#ifndef OPERAND_FACTORY_HPP_
#define OPERAND_FACTORY_HPP_


class OperandFactory
{

private:

	typedef IOperand* (OperandFactory::*CTOR)(std::string &);

	std::map<std::string, eOperandType>	_type;
	std::map<eOperandType, CTOR>		_create;
	std::map<eOperandType, long double> _max;
	std::map<eOperandType, long double> _min;

public:

	OperandFactory();

	~OperandFactory();

	eOperandType		getType(const std::string &);

	eOperandType		getRightType(const std::string &, const int, const int);

	int					getPrecision(const std::string &);

	std::string			truncatePrecision(std::string &, int, int);

	bool				isGoodValue(const eOperandType, const std::string &);

	IOperand*			createIOperand(eOperandType, std::string &);

	IOperand*			createInt8(std::string &);
	IOperand*			createInt16(std::string &);
	IOperand*			createInt32(std::string &);
	IOperand*			createFloat(std::string &);
	IOperand*			createDouble(std::string &);

};

#endif // OPERAND_FACTORY_HPP_
