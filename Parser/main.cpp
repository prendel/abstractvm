#include "Parser.hh"
#include "System_error.hh"

int main (int ac, char **av)
{
	try
	{
		if (ac < 2)
			Parser test2;
		else
			Parser test2(av[1]);
	}
	catch (System_error error)
	{
		std::cerr << error.what() << std::endl;
	}
    return (0);
}
