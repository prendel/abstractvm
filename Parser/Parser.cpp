#include "Parser.hh"

Parser::Parser(const std::string &filename): _filename(filename)
{
	_instructions = {"pop", "dump", "add", "sub", "mul", "div", "mod", "print", "exit"};
	_operators = {"push", "assert"};
	_types = {"int8", "int16", "int32", "float", "double"};
	openfile(_filename);
}

Parser::Parser()
{
	_instructions = {"pop", "dump", "add", "sub", "mul", "div", "mod", "print", "exit"};
	_operators = {"push", "assert"};
	_types = {"int8", "int16", "int32", "float", "double"};
	std::vector<_tab_line> tab;
	std::string tmp;
	bool comma = false;
	bool exit = false;
	size_t i;

	i = 0;
	while (!comma && getline(std::cin, tmp))
	{
		if (tmp == ";;" && !exit)
			throw System_error(MISSING_EXIT);
		else if (exit)
		{
			if (tmp != ";;")
				throw System_error(MISSING_COMMA);
			else
				comma = true;
		}
		tmp = epure_str(tmp);
		tmp = tmp.substr(0, tmp.find(';', 0));
		if (tmp == "exit" && !exit)
			exit = true;
		else
		{
			if (tmp.size() != 0)
				tab.push_back({tmp, i});
		}
		i++;
	}
	begin_parsing(tab);
}

Parser::~Parser()
{

}

int		Parser::openfile(const std::string &filename)
{
	std::ifstream	file;
	std::vector<_tab_line> tab;
	std::string		content;
	size_t			line;
	bool			exit = false;

	line = 1;
	file.open(_filename.c_str(), std::fstream::in | std::fstream::out);
	if (file.is_open())
	{
		while (getline(file, content) && !exit)
		{
			content = epure_str(content);
			//std::cout << '|' << content << '|' << std::endl;
			std::string string1 = content;
			string1 = string1.substr(0, string1.find(';', 0));
			if (content == "exit")
				exit = true;
			else if (string1.size() != 0)
			{
				//std::cout << "J'ajoute |" << string1 << "|" << std::endl;
				tab.push_back({string1, line});
			}
			line++;
		}
		if (!exit)
			throw System_error(MISSING_EXIT);
		else
			begin_parsing(tab);
	}
	else
		throw System_error(PRINT_FILE_NOT_FOUND(filename));
	return (0);
}

void Parser::begin_parsing(const std::vector<_tab_line> &tab)
{
	(void) tab;
	size_t i = 0;
	int 	j;
	std::vector<std::string> token;
	IOperand *result;
	OperandFactory	factory;
	Instructions instruction;

	while (i < tab.size())
	{
		//std::cout << "J'envoie ensuite |" << tab[i].str << "|" << std::endl;
		j = check_line(tab[i].str, tab[i].line);
		token = my_tok(tab[i].str);
		if (j == OPERATOR)
		{
			// if (token[0] == "push")
			// {
				result = factory.createIOperand(factory.getType(token[1]), token[2]);
				//std::cout << tab[i].str << "| j'envoie à Hugo" << std::endl;
				instruction.exec(token[0], result);
			// }
			// else if (token[0] == "assert")
			// 	std::cout << tab[i].str << "| j'envoie à qui le assert ?" << std::endl;
		}
		else if (j == INSTRUCTION)
		{
			instruction.exec(token[0]);
			//std::cout << token[0] << "J'envoie à Moritz" << std::endl;
		}
		i = i + 1;
	}
}

int	Parser::check_line(const std::string &str, const size_t &line)
{
	std::vector<std::string> token;

	token = my_tok(str);
	if (std::find(_instructions.begin(), _instructions.end(), token[0]) != _instructions.end())
	{
		if (token.size() > 1)
			throw System_error(PRINT_INVALID_NBR_ARG(token[0], line));
		return (INSTRUCTION);
	}
	else if (std::find(_operators.begin(), _operators.end(), token[0]) != _operators.end())
	{
		check_operators(token, line);
		return (OPERATOR);
	}
	else
		throw System_error(PRINT_COMMAND_NOT_FOUND(token[0], line));
	return (0);
}

std::vector<std::string>	Parser::my_tok(const std::string &str)
{
	std::vector<std::string> v;
	std::string string1;
	const char *line;
	size_t i = 0;

	line = str.c_str();
	while (i < str.size())
	{
		string1 = "";
		while (line[i] != ' ' && line[i] != '(' && line[i] != ')' && line[i] != '\n' && line[i])
		{
			string1 += line[i];
			i++;
		}
		v.push_back(string1);
		i++;
	}
	return (v);
}

int 	Parser::check_operators(const std::vector<std::string> &token, const size_t &line)
{
	std::vector<std::string>::iterator itr;

	if (token.size() != 3 || !((itr = std::find(std::begin(_types), std::end(_types), token[1])) != std::end(_types)))
	{
		if (token.size() >  2)
		{
			std::cout << token[1] << " | " << token[2] << " | " << token[3] << " | " << token[4] << std::endl;
			throw System_error(PRINT_WRONG_ARG(line, token[1]));
		}
		else
			throw System_error(PRINT_MISSING_ARG(line));
		return (-1);
	}
	size_t pos = itr - _types.begin();
	check_number(token, pos, line);
	return (0);
}

bool 	Parser::isdigitn(const std::string &str)
{
	for (size_t i = 0; i < str.size(); i++)
	{
		if (!i)
		{
			if (str[i] != '-' && !(str[i] >= '0' && str[i] <= '9'))
				return (false);
		}
		else if (str[i] < '0' || str[i] > '9')
			return (false);
	}
	return (true);
}

bool	Parser::isdigitz(const std::string &str)
{
	size_t i = 0;
	size_t point = 0;

	while (i < str.size())
	{
		if (!i)
		{
			if (str[i] != '-' && !(str[i] >= '0' && str[i] <= '9'))
				return (false);
		}
		else if (!(str[i] >= '0' && str[i] <= '9') && str[i] != '.')
			return (false);
		else if (str[i] == '.')
			point++;
		i = i + 1;
	}
	return (point == 1 ? true : false);
}

int 	Parser::check_number(const std::vector<std::string> &token, const size_t &pos, const size_t &line)
{
	if (pos == FLOAT || pos == DOUBLE)
	{
		if (!isdigitz(token[2]))
			throw System_error(PRINT_INVALID_NBR(token[2], line, _types[pos]));
	}
	else
	{
		if (!isdigitn(token[2]))
			throw System_error(PRINT_INVALID_NBR(token[2], line, _types[pos]));
	}
	return (0);
}

const char	*Parser::mylist(const std::vector<std::string> &list)
{
	std::string string1;

	string1 = "";
	for (std::vector<std::string>::const_iterator itr = list.begin(); itr != list.end(); itr++)
	{
		string1 += "'" + *itr + "'";
		if (itr + 1 != list.end())
			string1 += ",";
	}
	return (string1.c_str());
}

std::string Parser::epure_str(const std::string &str)
{
	size_t i;
	std::string string1;

	i = 0;
	string1 = "";
	while (i < str.size() && str[i] != ';')
	{
		if (str[i] == ' ')
		{
			while(str[i] == ' ' && i < str.size())
				i = i + 1;
			i = i - 1;
			string1 += ' ';			
		}
		else
			string1 += str[i];
		i = i + 1;
	}
	if (string1[string1.size() - 1] == ' ')
		string1.pop_back();
	if (string1[0] == ' ')
		string1.erase(0, 1);
	return (string1);
}