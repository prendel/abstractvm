#include <iostream>
#include <string>
#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "Float.hpp"
#include "../Parser/System_error.hh"

Float::Float(std::string const& value)
{
	OperandFactory	factory;

	if (factory.isGoodValue(eOperandType::FLOAT, value) == false)
		throw (System_error("Overflow/Underflow"));
	this->_value = std::stof(value);
	this->_string = value;
	this->_precision = factory.getPrecision(value);
}

std::string const & Float::toString() const
{
	return (this->_string);
}

int	Float::getPrecision() const
{
	return (this->_precision);
}

eOperandType	Float::getType() const
{
	return (eOperandType::FLOAT);
}

IOperand*			Float::operator+(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) + std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Float::operator-(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) - std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Float::operator*(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) * std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Float::operator/(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) / std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Float::operator%(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;
	double			value1;
	double			value2;
	double			tmp;

	value1 = std::stod(this->_string);
	value2 = std::stod(rhs.toString());
	tmp = value1;
	while (tmp >= value2)
		tmp -= value2;
	resultValue = std::to_string(tmp);
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}
