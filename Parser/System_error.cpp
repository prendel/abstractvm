//
// System_error.cpp for Abstractvm in /home/page_a/rendu/abstractvm/Parser
// 
// Made by Alexandre PAGE
// Login   <page_a@epitech.net>
// 
// Started on  Fri Feb 20 17:10:30 2015 Alexandre PAGE
// Last update Fri Feb 20 17:10:31 2015 Alexandre PAGE
//

#include "System_error.hh"

System_error::System_error(std::string error): _msg(error)
{

}

System_error::~System_error() throw()
{

}

const char *System_error::what() const throw()
{
	return (_msg.c_str());
}