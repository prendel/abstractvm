#include <string>
#include "IOperand.hpp"

#ifndef FLOAT_HPP_
#define FLOAT_HPP_

class Float : public IOperand
{

private:

	float		_value;
	std::string	_string;

public:

	Float(std::string const & value = "0");

	virtual ~Float(){};

	virtual std::string const & toString() const; // Renvoie une string reprensentant l’instance

	virtual int getPrecision() const; // Renvoie la precision du type de l’instance
	virtual eOperandType getType() const; // Renvoie le type de l’instance. Voir plus bas

	virtual IOperand* operator+(const IOperand &rhs) const; // Somme
	virtual IOperand* operator-(const IOperand &rhs) const; // Difference
	virtual IOperand* operator*(const IOperand &rhs) const; // Produit
	virtual IOperand* operator/(const IOperand &rhs) const; // Quotient
	virtual IOperand* operator%(const IOperand &rhs) const; // Modulo

};

#endif // FLOAT_HPP_