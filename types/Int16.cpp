#include <iostream>
#include <string>
#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "Int16.hpp"
#include "../Parser/System_error.hh"

Int16::Int16(std::string const& value)
{
	OperandFactory	factory;

	if (factory.isGoodValue(eOperandType::INT16, value) == false)
		throw (System_error("Overflow/Underflow"));
	this->_value = std::stoi(value, 0, 10);
	this->_string = value;
	this->_precision = 0;
}

std::string const & Int16::toString() const
{
	return (this->_string);
}

int	Int16::getPrecision() const
{
	return (this->_precision);
}

eOperandType	Int16::getType() const
{
	return (eOperandType::INT16);
}

IOperand*			Int16::operator+(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) + std::stod(rhs.toString()));
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Int16::operator-(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) - std::stod(rhs.toString()));
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Int16::operator*(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) * std::stod(rhs.toString()));
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Int16::operator/(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) / std::stod(rhs.toString()));
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Int16::operator%(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;
	double			value1;
	double			value2;
	double			tmp;

	value1 = std::stod(this->_string);
	value2 = std::stod(rhs.toString());
	tmp = value1;
	while (tmp >= value2)
		tmp -= value2;
	resultValue = std::to_string(tmp);
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}
