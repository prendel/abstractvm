#include <string>
#include "IOperand.hpp"

#ifndef INT32_HPP_
#define INT32_HPP_

class Int32 : public IOperand
{

private:

	int			_value;
	std::string	_string;

public:

	Int32(std::string const & value = "0");

	virtual ~Int32(){};

	virtual std::string const & toString() const; // Renvoie une string reprensentant l’instance

	virtual int getPrecision() const; // Renvoie la precision du type de l’instance
	virtual eOperandType getType() const; // Renvoie le type de l’instance. Voir plus bas

	virtual IOperand* operator+(const IOperand &rhs) const; // Somme
	virtual IOperand* operator-(const IOperand &rhs) const; // Difference
	virtual IOperand* operator*(const IOperand &rhs) const; // Produit
	virtual IOperand* operator/(const IOperand &rhs) const; // Quotient
	virtual IOperand* operator%(const IOperand &rhs) const; // Modulo

};

#endif // INT32_HPP_