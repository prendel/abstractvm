//
// instructions.cpp for abstract in /home/thomas/rendu/abstractvm/instruction
// 
// Made by Thomas MORITZ
// Login   <thomas@epitech.net>
// 
// Started on  Wed Feb 11 17:13:53 2015 Thomas MORITZ
// Last update Sat Feb 28 18:30:03 2015 Thomas MORITZ
//

#include <iostream>
#include <stdlib.h>
#include "../types/IOperand.hpp"
#include "../Parser/System_error.hh"
#include "instructions.hpp"

Instructions::Instructions()
{
	_mapnoarg["pop"] = &Instructions::pop;
	_mapnoarg["dump"] = &Instructions::dump;
	_mapnoarg["add"] = &Instructions::add;
	_mapnoarg["sub"] = &Instructions::sub;
	_mapnoarg["mul"] = &Instructions::mul;
	_mapnoarg["div"] = &Instructions::div;
	_mapnoarg["mod"] = &Instructions::mod;
	_mapnoarg["print"] = &Instructions::print;
	_mapnoarg["my_exit"] = &Instructions::my_exit;
	_maparg["push"] = &Instructions::push;
	_maparg["assert"] = &Instructions::assert;
}

Instructions::~Instructions()
{

}

void	Instructions::exec(std::string func)
{
  (this->*(_mapnoarg[func]))();
}

void	Instructions::exec(std::string func, IOperand *op)
{
  (this->*(_maparg[func]))(op);
}

void	Instructions::push(IOperand *val)
{
	this->_op.push_front(val);
}

void	Instructions::assert(IOperand *val)
{
	IOperand *tmp;

	tmp = this->_op.front();
	if (tmp->toString() != val->toString() || tmp->getType() != val->getType())
		exit (EXIT_FAILURE);
}

void	Instructions::add()
{
	IOperand *nbr1;
	IOperand *nbr2;

	if (this->_op.size() < 2)
	{
		throw System_error("Stack too short must have minus 2 Operands");
	}
	nbr1 = this->_op.front();
	this->_op.pop_front();
	nbr2 = this->_op.front();
	this->_op.pop_front();
	this->_op.push_front(*nbr1 + *nbr2);
}

void	Instructions::sub()
{
	IOperand *nbr1;
	IOperand *nbr2;

	if (this->_op.size() < 2)
	{
		throw System_error("Stack too short must have minus 2 Operands");
	}
	nbr1 = this->_op.front();
	this->_op.pop_front();
	nbr2 = this->_op.front();
	this->_op.pop_front();
	this->_op.push_front(*nbr1 - *nbr2);
}

void	Instructions::mul()
{
	IOperand *nbr1;
	IOperand *nbr2;

	if (this->_op.size() < 2)
	{
		throw System_error("Stack too short must have minus 2 Operands");
	}
	nbr1 = this->_op.front();
	this->_op.pop_front();
	nbr2 = this->_op.front();
	this->_op.pop_front();
	this->_op.push_front(*nbr1 * *nbr2);
}

void	Instructions::div()
{
	IOperand *nbr1;
	IOperand *nbr2;

	if (this->_op.size() < 2)
	{
		throw System_error("Stack too short must have minus 2 Operands");
	}
	nbr1 = this->_op.front();
	this->_op.pop_front();
	nbr2 = this->_op.front();
	this->_op.pop_front();
	if (nbr2->toString() == "0")
	{
		throw System_error("Division by 0 is impossible");
	}
	this->_op.push_front(*nbr1 / *nbr2);
}

void	Instructions::mod()
{
	IOperand *nbr1;
	IOperand *nbr2;

	if (this->_op.size() < 2)
	{
		throw System_error("Stack too short must have minus 2 Operands");
	}
	nbr1 = this->_op.front();
	this->_op.pop_front();
	nbr2 = this->_op.front();
	this->_op.pop_front();
	if (nbr2->toString() == "0")
	{
		throw System_error("Modulo by 0 is impossible");
	}
	this->_op.push_front(*nbr1 % *nbr2);
}


void	Instructions::dump()
{
	std::list<IOperand*>::iterator it;

	for (it = this->_op.begin(); it != this->_op.end(); it++)
		std::cout << (*it)->toString() << std::endl;
}

void	Instructions::pop()
{
	if (this->_op.size() == 0)
	{
		throw System_error("The list is empty");
	}
	this->_op.pop_front();
}

void	Instructions::print()
{
	IOperand *first;
	char		item;

	if (this->_op.size() == 0)
	{
		throw System_error("The list is empty");
	}
	else
	{
		first = this->_op.front();
		if (first->getType() != eOperandType::INT8)
			exit (EXIT_FAILURE);
		item = std::stoi(first->toString());
		std::cout << item << std::endl;
	}
}

void	Instructions::my_exit()
{
	exit (EXIT_FAILURE);
}
