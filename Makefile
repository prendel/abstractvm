##
## Makefile for AbstractVM in /home/page_a/rendu/abstractvm
## 
## Made by Alexandre PAGE
## Login   <page_a@epitech.net>
## 
## Started on  Wed Feb 11 12:37:22 2015 Alexandre PAGE
## Last update Thu Feb 19 16:44:08 2015 Alexandre PAGE
##

NAME	= avm

SRCS	= Parser/System_error.cpp \
		  Parser/Parser.cpp \
	  	  Parser/main.cpp \
	  	  types/Double.cpp \
	  	  types/Float.cpp \
	  	  types/Int16.cpp \
	  	  types/Int32.cpp \
	  	  types/Int8.cpp \
	  	  types/OperandFactory.cpp \
	  	  instruction/instructions.cpp

OBJS	= $(SRCS:.c=.o)

RM	= rm -f

GCC	= g++ -std=c++11

CFLAGS	+= -W -Wall -Wextra -Werror

$(NAME): $(OBJS)
	$(GCC) $(CFLAGS) $(OBJS) -o $(NAME)

all: $(NAME)

fclean:
	$(RM) $(NAME)

re: fclean all