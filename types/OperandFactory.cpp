#include <map>
#include <string>
#include <limits>
#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "Int8.hpp"
#include "Int16.hpp"
#include "Int32.hpp"
#include "Float.hpp"
#include "Double.hpp"

OperandFactory::OperandFactory()
{
	this->_type["int8"] = eOperandType::INT8;
	this->_type["int16"] = eOperandType::INT16;
	this->_type["int32"] = eOperandType::INT32;
	this->_type["float"] = eOperandType::FLOAT;
	this->_type["double"] = eOperandType::DOUBLE;

	this->_create[eOperandType::INT8] = &OperandFactory::createInt8;
	this->_create[eOperandType::INT16] = &OperandFactory::createInt16;
	this->_create[eOperandType::INT32] = &OperandFactory::createInt32;
	this->_create[eOperandType::FLOAT] = &OperandFactory::createFloat;
	this->_create[eOperandType::DOUBLE] = &OperandFactory::createDouble;

	this->_max[eOperandType::INT8] = std::numeric_limits<char>::max();
	this->_max[eOperandType::INT16] = std::numeric_limits<short>::max();
	this->_max[eOperandType::INT32] = std::numeric_limits<int>::max();
	this->_max[eOperandType::FLOAT] = std::numeric_limits<float>::max();
	this->_max[eOperandType::DOUBLE] = std::numeric_limits<double>::max();

	this->_min[eOperandType::INT8] = std::numeric_limits<char>::min();
	this->_min[eOperandType::INT16] = std::numeric_limits<short>::min();
	this->_min[eOperandType::INT32] = std::numeric_limits<int>::min();
	this->_min[eOperandType::FLOAT] = std::numeric_limits<float>::min();
	this->_min[eOperandType::DOUBLE] = std::numeric_limits<double>::min();
}

OperandFactory::~OperandFactory()
{
}

eOperandType	OperandFactory::getType(const std::string & typeString)
{
	return (this->_type[typeString]);
}

eOperandType	OperandFactory::getRightType(const std::string & valueString, const int precision1, const int precision2)
{
	if (precision1 > 0 || precision2 > 0)
		return ((precision1 > 7 || precision2 > 7) ? eOperandType::DOUBLE : eOperandType::FLOAT);
	else
		return (this->isGoodValue(eOperandType::INT8, valueString) ?
		eOperandType::INT8 :
		(this->isGoodValue(eOperandType::INT16, valueString) ?
			eOperandType::INT16 :
			(this->isGoodValue(eOperandType::INT32, valueString) ?
				eOperandType::INT32 :
				(this->isGoodValue(eOperandType::FLOAT, valueString) ?
					eOperandType::FLOAT : eOperandType::DOUBLE))));
}

int	OperandFactory::getPrecision(const std::string & value)
{
	std::string result;
	std::size_t pos;

	pos = value.find_first_of(".");
	if (pos == std::string::npos)
		return (0);
	result = value.substr(pos);
	return (result.size() - 1);
}

std::string	OperandFactory::truncatePrecision(std::string & value, int precision1, int precision2)
{
	int			precision;
	std::string result;
	std::size_t pos;

	precision = (precision1 < precision2 ? precision2 : precision1);
	pos = value.find_first_of(".");
	if (pos != std::string::npos)
	{
		if (precision != 0)
			result = value.substr(0, pos + precision + 1);
		else
			result = value.substr(0, pos);
	}
	else
		return (value);
	return (result);
}

bool	OperandFactory::isGoodValue(const eOperandType type, const std::string & value)
{
	if (std::stold(value) >= this->_min[type] && std::stold(value) <= this->_max[type])
		return (true);
	else
		return (false);
}

IOperand*	OperandFactory::createIOperand(eOperandType type, std::string & value)
{
	return ((this->*(_create[type]))(value));
}

IOperand*	OperandFactory::createInt8(std::string & value)
{
	return (new Int8(value));
}

IOperand*	OperandFactory::createInt16(std::string & value)
{
	return (new Int16(value));
}

IOperand*	OperandFactory::createInt32(std::string & value)
{
	return (new Int32(value));
}

IOperand*	OperandFactory::createFloat(std::string & value)
{
	return (new Float(value));
}

IOperand*	OperandFactory::createDouble(std::string & value)
{
	return (new Double(value));
}

