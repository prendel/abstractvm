#include <iostream>
#include <string>
#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "Double.hpp"
#include "../Parser/System_error.hh"

Double::Double(std::string const& value)
{
	OperandFactory	factory;

	if (factory.isGoodValue(eOperandType::DOUBLE, value) == false)
		throw (System_error("Overflow/Underflow"));
	this->_value = std::stod(value);
	this->_string = value;
	this->_precision = factory.getPrecision(value);
	//CHECK IF UNDER/OVER-FLOW
}

std::string const & Double::toString() const
{
	return (this->_string);
}

int	Double::getPrecision() const
{
	return (this->_precision);
}

eOperandType	Double::getType() const
{
	return (eOperandType::DOUBLE);
}

IOperand*			Double::operator+(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) + std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Double::operator-(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) - std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Double::operator*(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) * std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Double::operator/(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;

	resultValue = std::to_string(std::stod(this->_string) / std::stod(rhs.toString()));
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}

IOperand*			Double::operator%(const IOperand & rhs) const
{
	std::string		resultValue;
	eOperandType	resultType;
	OperandFactory	factory;
	double			value1;
	double			value2;
	double			tmp;

	value1 = std::stod(this->_string);
	value2 = std::stod(rhs.toString());
	tmp = value1;
	while (tmp >= value2)
		tmp -= value2;
	resultValue = std::to_string(tmp);
	resultValue = factory.truncatePrecision(resultValue, this->_precision, rhs.getPrecision());
	resultType = factory.getRightType(resultValue, this->_precision, rhs.getPrecision());
	return (factory.createIOperand(resultType, resultValue));
}
