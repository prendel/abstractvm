//
// instructions.hpp for abstract in /home/thomas/rendu/abstractvm/instruction
// 
// Made by Thomas MORITZ
// Login   <thomas@epitech.net>
// 
// Started on  Wed Feb 11 17:13:43 2015 Thomas MORITZ
// Last update Wed Feb 11 17:13:45 2015 Thomas MORITZ
//

#include <list>
#include <iostream>
#include <map>
#include "../types/IOperand.hpp"

class Instructions
{
	public:
		Instructions();
		~Instructions();
		void		push(IOperand*); /*OK*/
		void		pop(); /* OK */
		void		dump(); /* OK */
		void		assert(IOperand*);
		void		add(); /* OK */
		void		sub(); /* OK */
		void		mul(); /* OK */
		void		div(); /* OK */
		void		mod(); /* OK */
		void		print(); /* OK */
		void		my_exit();
		void		exec(std::string);
		void		exec(std::string, IOperand*);
	private:

		typedef void (Instructions::*PTR)();
		typedef void (Instructions::*PTR2)(IOperand*);

		std::list<IOperand*> _op;
		std::map<std::string, PTR> _mapnoarg;
		std::map<std::string, PTR2> _maparg;
};