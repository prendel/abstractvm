//
// System_error.hh for Abstractvm in /home/page_a/rendu/abstractvm/Parser
// 
// Made by Alexandre PAGE
// Login   <page_a@epitech.net>
// 
// Started on  Fri Feb 20 17:04:08 2015 Alexandre PAGE
// Last update Fri Feb 20 17:04:09 2015 Alexandre PAGE
//

#ifndef SYSTEM_ERROR_HH_
# define SYSTEM_ERROR_HH_

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <string.h>

class System_error: public std::exception
{
public:
	~System_error() throw();// {}
	System_error(std::string error); //: _msg(error) {};
	const char *what() const throw();// {
//		return _msg.c_str(); 
//	}

private:
	std::string _msg;
};

#endif /* !SYSTEM_ERROR_HH_ */